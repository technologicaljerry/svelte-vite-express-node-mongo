import { Router } from "express";
import { createUser } from "../controllers/user.controller";

export const userRoute = () => {
  const router = Router();

  router.post("/users", createUser);

  return router;
};
